# Flickr Downloader

A simple Python script to download images from Flickr.
Takes a list of Flickr image URLs as argument.

## Usage

`flicker-downloader.py [-OPTIONS] URLS`
    
### Options:

```
-h, --help                show this help and exit
-v, --verbose             verbose output
-f FILE, --file=FILE      load URLs from file
-d, --delete              delete URL file after download
-p, --prefix              download directory
-s, --size                desired size to download```