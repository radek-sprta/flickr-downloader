#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
A simple script to download image from its Flickr page.
Takes a list of Flickr URLs as argument.

@author: Radek SPRTA
"""

import urllib, re, sys, getopt, os
from urllib2 import Request, urlopen, URLError
from BeautifulSoup import BeautifulSoup

verbose=False
delete=False
filename=""
prefix=""
size="o"

sizes = {
    "Square 75" : "sq",
    "Square 150" : "q",
    "Thumbnail" : "t",
    "Small 240" : "s",
    "Small 320" : "n",
    "Medium 500" : "m",
    "Medium 640" : "z",
    "Medium 800" : "c",
    "Large 1024" : "l",
    "Large 1600" : "h",
    "Large 2048" : "k",
    "Original" : "o"
}

def usage():
    usage="""
    Usage:
    flicker-downloader.py [-OPTIONS] URLS
    
    Options:
    -h, --help                show this help and exit
    -v, --verbose             verbose output
    -f FILE, --file=FILE      load URLs from file
    -d, --delete              delete URL file after download
    -p, --prefix              download directory
    -s, --size                desired size to download
    """
    print usage

def validateUrl(url):
    req = Request(url)
    try:
        urlopen(req)
    except URLError, e:
        if hasattr(e, "reason"):
            if verbose:
                print url, ":"
                print "We failed to reach a server."
                print "Reason:", e.reason
            return False
        elif hasattr(e, 'code'):
            if verbose:           
                print url, ":"
                print "The server couldn't fulfill the request."
                print "Error code:", e.code
            return False
    except ValueError, e:
        if verbose:
            print url, ":"
            print "URL is invalid."
        return False
    else:
        if verbose:
            print url, ":"
            print "URL is valid."
    return True

def validatePrefix(prefix):
    if prefix is not "":
        try:
            if not os.path.isdir(prefix):
                os.mkdir(prefix)
        except OSError:
            if verbose:
                print "Failed to open directory", prefix
            return False
    return True
            
def getImageUrl(url):
    url = "http://www.flickr.com/photos/" + getImageID(url) + "/sizes/" + size + "/"
    url = urllib.urlopen(url)
    page = BeautifulSoup(url)
    result = page.find(id="allsizes-photo").img.get("src")
    return result

def getImageName(url):
    match = re.search(r".*/(.*\.jpg)", url)
    imagename = match.group(1)
    return imagename
    
def downloadImage(url):
    image = urllib.URLopener()
    filename = getImageName(url)
    if validatePrefix(prefix):
        filename = os.path.abspath(prefix + filename)                    
        image.retrieve(url, filename)
    else:
        print "Invalid directory", prefix
        usage()
        return False
    return True    

def parseURLFile(filename):
    urls = [] 
    try:
        f = open(filename)
        try:
            urls = f.readlines()
        finally:
            f.close()
        if delete:
            os.remove(os.path.join(os.getcwd(), filename))
            if verbose:
                print filename, "deleted"
    except IOError:
        print "Failed to open", filename
        sys.exit(2)
    return urls

def getImageID(url):
    match = re.search(r"https?://www.flickr.com/photos/(\w+?/\d+?)/", url)
    imageid = ""
    if match:
        imageid = match.group(1)
    return imageid


try:
    opts, args = getopt.getopt(sys.argv[1:], "hvf:dp:s:", ["help", "verbose", "file=", "delete", "prefix=", "size="])
except getopt.GetoptError:
    usage()
    sys.exit(2)
for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit()
    elif opt in ("-v", "--verbose"):
        verbose = True
    elif opt in ("-d", "--delete"):
        delete = True
    elif opt in ("-f", "--file"):
        filename = arg
    elif opt in ("-p", "--prefix"):
        prefix = arg + "/"
    elif opt in ("-s", "--size"):
        if arg in sizes:
            size = sizes[arg]
        else:
            print "No size '" + arg + "', falling back to original"
if filename is not "":
    args = args + parseURLFile(filename)
if args == []:
    print "No URLs to download from"
    usage()
    sys.exit(2)
for arg in args:
    if validateUrl(arg):
        imageurl = getImageUrl(arg)
        imagename = getImageName(imageurl)
        if downloadImage(imageurl):
            print "Successfully downloaded", imagename, "from", arg
        else:
            print "Failed to download from", arg
